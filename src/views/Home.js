import React, { useEffect, useState } from 'react';
// import sections
import Hero from '../components/sections/Hero';
import FeaturesTiles from '../components/sections/FeaturesTiles';
import FeaturesSplit from '../components/sections/FeaturesSplit';
import Testimonial from '../components/sections/Testimonial';
import Cta from '../components/sections/Cta';
import Popup from '../components/elements/Popup';

const Home = () => {
  const [showPopup,setShowPopup] = useState(false)

  useEffect(() => {
    const timeout = setTimeout(() => {
      setShowPopup(true)
    },2000)
    return () => {
      clearTimeout(timeout)
    }
  },[])

  return (
    <>
      {/* <Hero className="illustration-section-01" /> */}
      {/* <FeaturesTiles /> */}
      {showPopup && <Popup closePopup={() => setShowPopup(false)} />}
      <FeaturesSplit invertMobile topDivider imageFill className="illustration-section-02" />
      <Testimonial topDivider />
      <Cta split />
    </>
  );
}

export default Home;