import React from 'react'
import { Link } from 'react-router-dom';

export default function Popup({ closePopup }) {
    return (
        <div className="pop-up">
            <div>
                <h1>Get bonus in real life?</h1>
                <div className="button-container">
                    <button >YES</button>
                    <button onClick={closePopup}>NO</button>
                </div>
            </div>
        </div>
    )
}
