import React from 'react';
import classNames from 'classnames';
import { SectionSplitProps } from '../../utils/SectionProps';
import SectionHeader from './partials/SectionHeader';
import Image from '../elements/Image';

const propTypes = {
  ...SectionSplitProps.types
}

const defaultProps = {
  ...SectionSplitProps.defaults
}

const FeaturesSplit = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  invertMobile,
  invertDesktop,
  alignTop,
  imageFill,
  ...props
}) => {

  const outerClasses = classNames(
    'features-split section',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'features-split-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );

  const splitClasses = classNames(
    'split-wrap',
    invertMobile && 'invert-mobile',
    invertDesktop && 'invert-desktop',
    alignTop && 'align-top'
  );

  const sectionHeader = {
    title: 'THE MOST PLAYER-CENTRIC',
    paragraph: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum — semper quis lectus nulla at volutpat diam ut venenatis.'
  };

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader data={sectionHeader} className="center-content" />
          <div className={splitClasses}>

            <div className="split-item">
              <div className="split-item-content center-content-mobile reveal-from-left" data-reveal-container=".split-item">

                <h3 className="mt-0 mb-12">
                  CASH-FRENZY
                </h3>
                <p className="m-0">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua — Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <div className="split-item" style={{width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <Image
                    src={require('./../../assets/images/google-play-badge.png')}
                    alt="Google play"
                    width={220}
                    // height={200} 
                  />
                  <Image
                    src={require('./../../assets/images/app-store-badge.svg')}
                    alt="Google play"
                    width={194}
                    // height={100} 
                  />
                </div>
              </div>
              <div className={
                classNames(
                  'split-item-image center-content-mobile reveal-from-bottom',
                  imageFill && 'split-item-image-fill'
                )}
                data-reveal-container=".split-item">
                <a target={"_blank"} href={`https://demogamesfree.ppgames.net/gs2c/html5Game.do?extGame=1&symbol=vs20olympgate&gname=Gates%20of%20Olympus&jurisdictionID=UK&lobbyUrl=https%3A%2F%2Fwww.188bet.com%2Fvi-vn%2Fcasino%2Fvs20olympgate&mgckey=stylename@188bt_188bet~SESSION@50bc4afb-2157-4e24-81f7-489ee9458fd2`}>
                  <Image
                    src={require('./../../assets/images/features-split-image-01.png')}
                    alt="Features split 01"
                    width={528}
                    height={396} />
                </a>
              </div>
            </div>

            <div className="split-item">
              <div className="split-item-content center-content-mobile reveal-from-right" data-reveal-container=".split-item">

                <h3 className="mt-0 mb-12">
                  CASH-FRENZY
                </h3>
                <p className="m-0">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua — Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <div className="split-item" style={{width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <Image
                    src={require('./../../assets/images/google-play-badge.png')}
                    alt="Google play"
                    width={220}
                    // height={200} 
                  />
                  <Image
                    src={require('./../../assets/images/app-store-badge.svg')}
                    alt="Google play"
                    width={194}
                    // height={100} 
                  />
                </div>
              </div>
              <div className={
                classNames(
                  'split-item-image center-content-mobile reveal-from-bottom',
                  imageFill && 'split-item-image-fill'
                )}
                data-reveal-container=".split-item">
                <a target={"_blank"} href={`https://demogamesfree.ppgames.net/gs2c/html5Game.do?extGame=1&symbol=vs20olympgate&gname=Gates%20of%20Olympus&jurisdictionID=UK&lobbyUrl=https%3A%2F%2Fwww.188bet.com%2Fvi-vn%2Fcasino%2Fvs20olympgate&mgckey=stylename@188bt_188bet~SESSION@50bc4afb-2157-4e24-81f7-489ee9458fd2`}>
                  <Image
                    src={require('./../../assets/images/features-split-image-02.png')}
                    alt="Features split 02"
                    width={528}
                    height={396} />
                </a>
              </div>
            </div>

            <div className="split-item">
              <div className="split-item-content center-content-mobile reveal-from-left" data-reveal-container=".split-item">

                <h3 className="mt-0 mb-12">
                  CASH-FRENZY
                </h3>
                <p className="m-0">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua — Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <div className="split-item" style={{width: "100%", display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                  <Image
                    src={require('./../../assets/images/google-play-badge.png')}
                    alt="Google play"
                    width={220}
                    // height={200} 
                  />
                  <Image
                    src={require('./../../assets/images/app-store-badge.svg')}
                    alt="Google play"
                    width={194}
                    // height={100} 
                  />
                </div>
              </div>
              <div className={
                classNames(
                  'split-item-image center-content-mobile reveal-from-bottom',
                  imageFill && 'split-item-image-fill'
                )}
                data-reveal-container=".split-item">
                <a target={"_blank"} href={`https://static-common-sg.isbtechno.com/games/html/html5/pulse_fu_fortunes_megaways/pulse_fu_fortunes_megaways_r28/pulse_fu_fortunes_megaways.html?name=232,fun&password=fun&lang=en&currency=EUR&funmode=true&rulesUrl=https%3A%2F%2Fstatic-common-sg.isbtechno.com%2Fgames%2Fhtml%2Fhtml5%2Frules%2Fen%2Fpulse_fu_fortunes_megaways_rules.html%3Flid%3D232%26country%3DVN&skinid=200200&channelautodetection=ON&allowFullScreen=true&cachebuster=ce4b6f0258158531ed55ff758868d877e1797524&enableConsole=false&newSkinIDFormat=true&gameLinkPOSTcontent=&licenseId=232&operator=0&providerId=1&identifier=pulse_fu_fortunes_megaways&cur=EUR&historyURL=&lobbyURL=&turboMode=false&revision=&country=VN&environment_type=production&environment_domain=singapore.isoftbet.com&server_id=18&userId=&username=&token=&gapLauncherScriptExtension=null&italy_aams_id=null&italy_participation_id=null&italy_rebuy_icon=false&mode=0`}>
                  <Image
                    src={require('./../../assets/images/features-split-image-03.png')}
                    alt="Features split 03"
                    width={528}
                    height={396} />
                </a>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  );
}

FeaturesSplit.propTypes = propTypes;
FeaturesSplit.defaultProps = defaultProps;

export default FeaturesSplit;